# Static

This repository is meant to provide a source for all static files that can be
used in multiple projects.  For example, jQuery and Bootstrap are portable to
any project and thus are included here.

## Development Use

In development, feel free to reference the repo's scripts directly.

Example:

<script src="https://gitlab.com/jordangumm/static/raw/master/js/jquery-1.11.3.min.js" type="text/javascript"></script>
